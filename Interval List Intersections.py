from typing import List


class Solution:
    def intervalIntersection(self, firstList: List[List[int]], secondList: List[List[int]]) -> List[List[int]]:

        if len(firstList) == 0 or len(secondList) == 0:
            return []

        result = []

        i = 0
        j = 0

        while len(firstList) > i and len(secondList) > j:
            i_min, i_max = firstList[i]
            j_min, j_max = secondList[j]

            minimum = max(i_min, j_min)
            maximum = min(i_max, j_max)

            if minimum <= maximum:
                result.append([minimum, maximum])

            if i_max <= j_max:
                i += 1
            elif i_max >= j_max:
                j += 1

        return result


a = Solution()
print(a.intervalIntersection(firstList=[[0, 2], [5, 10], [13, 23], [24, 25]],
                             secondList=[[1, 5], [8, 12], [15, 24], [25, 26]]))
