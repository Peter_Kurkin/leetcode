from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        d = {}
        for i, x in enumerate(nums):
            diff = target - x
            if diff in d:
                return [d[diff], i]
            else:
                d[x] = i


a = Solution()
print(a.twoSum([2, 7, 11, 15], 9))
print(a.twoSum([3, 2, 4], 6))
print(a.twoSum([3, 3], 6))
