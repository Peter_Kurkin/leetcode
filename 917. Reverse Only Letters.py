class Solution:
    def reverseOnlyLetters(self, s: str) -> str:
        if len(s) == 1:
            return s

        l = 0
        r = len(s) - 1

        s = list(s)
        while l <= r:
            if ('a' <= s[l] <= 'z' or 'A' <= s[l] <= 'Z') and ('a' <= s[r] <= 'z' or 'A' <= s[r] <= 'Z'):
                s[l], s[r] = s[r], s[l]
                l += 1
                r -= 1

            if not ('a' <= s[l] <= 'z' or 'A' <= s[l] <= 'Z'):
                l += 1

            if not ('a' <= s[r] <= 'z' or 'A' <= s[r] <= 'Z'):
                r -= 1

        return ''.join(s)


a = Solution()
# "dc-ba"
print(a.reverseOnlyLetters(s="ab-cd"))
# "a-bC-dEf-ghIj"
print(a.reverseOnlyLetters(s="a-bC-dEf-ghIj"))
# "Qedo1ct-eeLg= ntse-T!"
print(a.reverseOnlyLetters(s="Test1ng-Leet=code-Q!"))
