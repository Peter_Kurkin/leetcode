from typing import List


class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        # [1,1,1,3,3,4,3,2,4,2]
        dictionary = {}
        for x in nums:
            if x in dictionary:
                return True
            else:
                dictionary[x] = 1
