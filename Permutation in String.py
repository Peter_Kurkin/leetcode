class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        dictionary = {}
        for k in s1:
            if k in dictionary:
                dictionary[k] += 1
            else:
                dictionary[k] = 1

        d_2 = {}
        start = 0
        for i in range(len(s2)):

            if d_2 == dictionary:
                return True

            if s2[i] in dictionary:
                if s2[i] in d_2:
                    if d_2[s2[i]] < dictionary[s2[i]]:
                        d_2[s2[i]] += 1
                    elif s2[start] == s2[i]:
                        start += 1
                        continue
                    else:
                        start = i
                        d_2 = {}
                else:
                    d_2[s2[i]] = 1
            else:
                start = i
                d_2 = {}

        return True if d_2 == dictionary else False


a = Solution()
print(a.checkInclusion(s1="ab", s2="eidbaooo"))
print(a.checkInclusion(s1="ab", s2="eidboaoo"))
print(a.checkInclusion(s1="adc", s2="dcda"))
print(a.checkInclusion(s1="hello", s2="ooolleoooleh"))
print(a.checkInclusion(s1="abc", s2="ccccbbbbaaaa"))
