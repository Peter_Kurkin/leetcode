class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        # Ввод: s = "анаграмма", t = "нагарам"
        # Вывод: true
        if len(s) != len(t):
            return False

        countS, countT = {}, {}

        for i in range(len(s)):
            countS[s[i]] = 1 + countS.get(s[i], 0)
            countT[t[i]] = 1 + countT.get(t[i], 0)
        return countS == countT

ob = Solution()
print(ob.isAnagram("anagram", "nagaram"))
print(ob.isAnagram("a", "ab"))
print(ob.isAnagram("a", "aa"))
