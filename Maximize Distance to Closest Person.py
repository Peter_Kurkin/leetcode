from typing import List


class Solution:
    def maxDistToClosest(self, seats: List[int]) -> int:
        # maximum, current, first = 0, 0, None
        #
        # for seat in seats:
        #     if seat == 1:
        #         if first == None:
        #             first = current
        #
        #         maximum = max(current, maximum)
        #         current = 0
        #
        #     current += 1
        #
        # maximum = maximum // 2
        #
        # return max(first, maximum, current - 1)

        result = []
        one = 0
        two = 0
        for i, seat in enumerate(seats):
            if seat == 1:
                one, two = i, one

                if two == 0 and seats[0] == 0:
                    result.append(2 * (one - two))
                else:
                    result.append(one - two)

        if seats[-1] == 0:
            result.append((len(seats) - 1 - one) * 2)

        return max(result) // 2


a = Solution()
# 2
print(a.maxDistToClosest(seats=[1, 0, 0, 0, 1, 0, 1]))
# 3
print(a.maxDistToClosest(seats=[1, 0, 0, 0]))
# 1
print(a.maxDistToClosest(seats=[0, 1]))
# 1
print(a.maxDistToClosest(seats=[1, 1, 0, 1, 1]))
# 2
print(a.maxDistToClosest(seats=[0, 0, 1, 0, 1, 1]))
# 7
print(a.maxDistToClosest(seats=[0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]))
