class Solution:
    def romanToInt(self, s: str) -> int:
        d = {
            "I": 1,
            "V": 5,
            "X": 10,
            "L": 50,
            "C": 100,
            "D": 500,
            "M": 1000,
        }

        result = 0
        i = 0
        # s[i] = "I"
        # d[s[i]] = 100
        while i < len(s):
            if (i + 1 < len(s)) and d[s[i]] < d[s[i + 1]]:
                result += d[s[i + 1]] - d[s[i]]
                i += 2
            else:
                result += d[s[i]]
                i += 1

        return result


a = Solution()
print(a.romanToInt(s="III"))
print(a.romanToInt(s="LVIII"))
print(a.romanToInt(s="MCMXCIV"))
