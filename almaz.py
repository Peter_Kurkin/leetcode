# encode("AAAABBBC") => "A4B3C"
# encode("AAAA BBBCAAA") => "A4B3CA3"

def main(l: str):
    k = 1
    result = ''
    for a in range(len(l) - 1):
        if l[a] != ' ':
            b = l[a]
            if l[a] == l[a + 1]:
                k += 1
            else:
                if k > 1:
                    result += f'{b}{k}'
                else:
                    result += f'{b}'
                k = 1

    if k > 1:
        result += f'{l[-1]}{k}'
    else:
        result += f'{l[-1]}'

    return result


if __name__ == '__main__':
    print(main('AAAABBBC'))
    print(main('AAAA BBBCAAA'))
    print(main('AAAACBBBCAAAB'))
