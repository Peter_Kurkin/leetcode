from typing import List


class Solution:
    def compress(self, chars: List[str]) -> int:
        k = 1
        result = []
        for a in range(len(chars) - 1):
            b = chars[a]
            if chars[a] == chars[a + 1]:
                k += 1
            else:
                if k > 1:
                    result.append(b)
                    result.extend([i for i in str(k)])
                else:
                    result.append(b)
                k = 1

        if k > 1:
            result.append(chars[-1])
            result.extend([i for i in str(k)])
        else:
            result.append(chars[-1])

        return result


if __name__ == '__main__':
    a = Solution()
    # ["a","2","b","2","c","3"]
    print(a.compress(chars=["a", "a", "b", "b", "c", "c", "c"]))
    # ['a']
    print(a.compress(chars=["a"]))
    # ["a","b","1","2"].
    print(a.compress(chars=["a", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"]))
    # ["a","2","b","2","c","3"]
    print(a.compress(chars=["a", "a", "b", "b", "c", "c", "c"]))
